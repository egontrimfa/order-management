import { Router } from "express";
import { Cart, CartItem, Order, OrderItem } from "../db";
import { AltCartInterface, AltCartItemInterface } from "../interfaces";
import { authorization } from "../middlewares/authorization";

const router: Router = new (Router as any)()

router.post('/', authorization, async (req, res, next) => {
    try {
        const { email }: AltCartInterface = req.body
        const order = await Order.create({ email })
        const cart = await Cart.findOne({ where: { email }, attributes: { include: ['id'] } })

        if (!cart) {
            return res.status(404).send({ message: 'There is no cart for the given email yet!' })
        }

        const cartItems = await CartItem.findAll({ where: { cartId: cart.id } })

        if (!cartItems || cartItems.length < 1) {
            return res.status(422).send({ message: 'You do not have any items in your cart!' })
        }

        cartItems.forEach(async (cartItem) => {
            await OrderItem.create({ sku: cartItem.sku, price: cartItem.price, quantity: cartItem.quantity, orderId: order.id })
        })

        await Cart.destroy({ where: { email } })

        return res.status(201).send({ message: 'Order created!' })
    } catch (error) {
        next(error)
    }
})

export { router }