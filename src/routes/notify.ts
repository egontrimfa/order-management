import { Router } from "express";
import { NotifyInterface } from "../interfaces";
import { authorization } from "../middlewares/authorization";
import { registerNotification } from "../services";

const router: Router = new (Router as any)()

router.post('/', authorization, async (req, res, next) => {
    try {
        const { email, sku }: NotifyInterface = req.body

        await registerNotification(email, sku)

        return res.status(201).send()
    } catch (error) {
        next(error)
    }
})

export { router }