import { Router } from "express";

import { Cart, CartItem } from "../db";
import { AltCartItemInterface, EmailInterface, ItemExtendedInterface } from "../interfaces";
import { authorization } from "../middlewares/authorization";
import { moveProductType } from "../services";

const router: Router = new (Router as any)()

router.post('/', authorization, async (req, res, next) => {
    try {
        const { email }: EmailInterface = req.body

        const [cart, created] = await Cart.findOrCreate({
            where: { email }
        })

        return res.status(created ? 201 : 204).send()
    } catch (error) {
        next(error)
    }
})

router.get('/', authorization, async (req, res, next) => {
    try {
        const { email }: EmailInterface = req.body

        const [cart, cartCreated] = await Cart.findOrCreate(
            {
                where: { email },
                include: { model: CartItem, attributes: { exclude: ['id', 'createdAt', 'updatedAt', 'cartId'] } },
                attributes: { exclude: ['id', 'createdAt', 'updatedAt'] }
            }
        )

        return res.send(cart)
    } catch (error) {
        next(error)
    }
})

router.put('/item', authorization, async (req, res, next) => {
    try {
        const { email, items }: ItemExtendedInterface = req.body

        if (!items || !items.length) {
            return res.status(400).send({ message: 'Malformed request. No items provided!' })
        }

        const [cart, cartCreated] = await Cart.findOrCreate({
            where: { email }
        })

        for (const item of items) {
            const { sku, price, quantity } = item

            let [cartItem, cartItemCreated] = await CartItem.findOrCreate({
                where: { cartId: cart.id, sku },
                defaults: {
                    sku, price, quantity, cartId: cart.id
                }
            })
    
            if (cartItemCreated) {
                await moveProductType(sku, - quantity)
            } else {
                await moveProductType(sku, Number(cartItem.quantity) - Number(quantity))
    
                cartItem.quantity = quantity
                cartItem = await cartItem.save()
            }
        }

        return res.status(200).send()
    } catch (error) {
        // We should use a transaction to roll back every DB change in case of an exception
        // it is expecially important if the exception happens on the first time, when the cart item is created
        // it could lead to unexpected behavior in the product catalog db
        next(error)
    }
})

router.delete('/item', authorization, async (req, res, next) => {
    const { email, sku }: ItemExtendedInterface = req.body

    const cart = await Cart.findOne({
        where: { email }
    })

    if (!cart) {
        return res.status(404).send({ message: 'There is no cart with the given email address!' })
    }

    let cartItem = await CartItem.findOne({
        where: { cartId: cart.id, sku },
    })

    if (!cartItem) {
        return res.status(404).send({ message: 'This users cart does not have a product type with the given sku!' })
    }

    await moveProductType(sku, cartItem.quantity)
    await cartItem.destroy()

    return res.send()
})

export { router }