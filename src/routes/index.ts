import { Application } from 'express';
import { router as cartRouter } from './cart';
import { router as orderRouter } from './order';
import { router as notifyRouter } from './notify';

export const mountRoutes = (app: Application) => {
    app.use('/cart', cartRouter)
    app.use('/order', orderRouter)
    app.use('/notify', notifyRouter)
}