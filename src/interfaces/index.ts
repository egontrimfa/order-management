import { CreationOptional, ForeignKey, InferAttributes, InferCreationAttributes, Model } from "sequelize/types"

export interface EmailInterface {
    email: String
}

export interface ItemExtendedInterface {
    email: String,
    sku: String,
    price: Number,
    quantity: Number,
    items: AltCartItemInterface[]
}

export interface CartInterface extends Model<InferAttributes<CartInterface>, InferCreationAttributes<CartInterface>> {
    id: CreationOptional<number>,
    email: String,
    createdAt?: Date,
    updatedAt?: Date
}

export interface CartItemInterface extends Model<InferAttributes<CartItemInterface>, InferCreationAttributes<CartItemInterface>> {
    id: CreationOptional<number>,
    sku: String,
    price: Number,
    quantity: Number,
    createdAt?: Date,
    updatedAt?: Date,
    cartId: ForeignKey<number>
}

export interface OrderInterface extends Model<InferAttributes<OrderInterface>, InferCreationAttributes<OrderInterface>> {
    id: CreationOptional<number>,
    email: String,
    createdAt?: Date,
    updatedAt?: Date
}

export interface OrderItemInterface extends Model<InferAttributes<OrderItemInterface>, InferCreationAttributes<OrderItemInterface>> {
    id: CreationOptional<number>,
    sku: String,
    price: Number,
    quantity: Number,
    createdAt?: Date,
    updatedAt?: Date,
    orderId: ForeignKey<number>
}

export interface AltCartItemInterface {
    sku: String,
    price: Number,
    quantity: Number
}

export interface AltCartInterface {
    email: String,
    cart_items: AltCartItemInterface[]
}

export interface NotifyInterface {
    email: String,
    sku: String
}