import axios from "axios";
import HttpError from 'http-errors';

export const moveProductType = async (sku: String, quantity: Number) => {
    try {
        await axios.put('http://productcatalog-service/product/type/move', { sku, quantity })
    } catch (error: any) {
        if (error.response) {
            throw HttpError(error.response.status, error.response.data?.error)
        }
    
        if (error.request) {
            throw HttpError(500, 'No response was received.')
        }
    
        throw HttpError(500, error.message)
    }
}

export const registerNotification = async (email: String, sku: String) => {
    try {
        await axios.post('http://customernotification-service/subscribe', { email, sku })
    } catch (error: any) {
        if (error.response) {
            throw HttpError(error.response.status, error.response.data?.error)
        }
    
        if (error.request) {
            throw HttpError(500, 'No response was received.')
        }
    
        throw HttpError(500, error.message)
    }
}