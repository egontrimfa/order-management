import { Sequelize, DataTypes } from 'sequelize';
import { CartInterface, CartItemInterface, OrderInterface, OrderItemInterface } from '../interfaces';

const { DB_PROTOCOL, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME } = process.env
const sequelize = new Sequelize(`${DB_PROTOCOL}://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}/${DB_NAME}`)

export const Cart = sequelize.define<CartInterface>('cart', {
    id: {
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    }
})

export const CartItem = sequelize.define<CartItemInterface>('cart_item', {
    id: {
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true
    },
    sku: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
            min: 0.01
        }
    },
    quantity: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
            min: 1
        }
    },
    cartId: {
        type: DataTypes.INTEGER.UNSIGNED,
        references: {
            model: Cart,
            key: 'id'
        }
    }
})

Cart.hasMany(CartItem, {
    onDelete: 'CASCADE'
})
CartItem.belongsTo(Cart)

export const Order = sequelize.define<OrderInterface>('order', {
    id: {
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    }
})

export const OrderItem = sequelize.define<OrderItemInterface>('order_item', {
    id: {
        primaryKey: true,
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true
    },
    sku: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
            min: 0.01
        }
    },
    quantity: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
            min: 1
        }
    },
    orderId: {
        type: DataTypes.INTEGER.UNSIGNED,
        references: {
            model: Order,
            key: 'id'
        }
    }
})

Order.hasMany(OrderItem, {
    onDelete: 'CASCADE'
})
OrderItem.belongsTo(Order)

export const initializeAndSyncDB = async () => {
    try {
        // await sequelize.sync({ force: true })
        await sequelize.authenticate()
        console.log('Successfully connected to the OM DB (PostgeSQL)');
    } catch (error) {
        console.error(error);
    }
}
