import axios from 'axios';
import { Request, Response, NextFunction } from 'express';

export const authorization = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const access_token = req.headers.authorization

        if (!access_token || (access_token && !(/^Bearer .+$/.test(access_token)))) {
            return res.status(401).send({ message: 'Access token is missing or it is not a correct bearer token!' })
        }
    
        const response = await axios.get('http://usermanagement-service/authorize', {
            headers: {
                'Authorization': access_token
            }
        })

        if (!response || !response.data || !response.data.user) {
            return res.status(500).send({ message: "The user could not be validated or it's data could not be fetched!" })
        }

        req.body.email = response.data.user.email
    
        next() 
    } catch (error) {
        next(error)
    }
}