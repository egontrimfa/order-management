import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import json from 'morgan-json';
import Logger from './config/logger';

// Environment import and configuration
dotenv.config()

import { initializeAndSyncDB } from './db';
import { errorHandler } from './middlewares/error-handler';
import { mountRoutes } from './routes';

// Express configuration
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

morgan.token('body', req => JSON.stringify((req as any).body))
const format = json({
    origin: ':req[origin]',
    body: ':body',
    url: ':url',
    method: ':method',
    status: ':status',
    contentLength: ':res[content-length]',
    responseTime: ':response-time',
    totalTime: ':total-time'
})
app.use(morgan(format, { stream: { write: (message) => Logger.http(message) } }))

app.use(cors())

// Server configuration
const PORT = process.env.PORT || 8003

// Initialize the PostgreSQL (Order Management DB) connection and re-sync it
initializeAndSyncDB()

// Initiate application routes
mountRoutes(app)

// HTTP(S) error handler
app.use(errorHandler)

// Listening to requests
app.listen(PORT, () => { console.log('Listening...'); })